import os
import shutil

import pytest

from autonmt.configs import ConfigError, GlobalConfig, ScriptsConfig, StageConfig
from autonmt.corpus import Corpora, FilenameGenerator
from autonmt.stages import DataQuery, Stage, Script, BuildVocab
from autonmt.stages.data_query import DataTransferMethod, DataTransferUnitDependencyInjector, LanguagePair, MissingDataError, SimpleDataTransferUnit, TruncatingDataTransferUnit, is_valid_file
from autonmt.stages.merge import Merge
from autonmt.util import SubprocessRunnerDependencyInjector
from tests.constants import Constants
from tests.fixtures.corpus import corpus

@pytest.fixture
def global_config():
    return GlobalConfig(Constants.exp, None, None, None, None)

class InstantiableStage(Stage):
    def run(self):
        pass

@pytest.fixture
def base_stage(filled_corpora):
    return InstantiableStage(filled_corpora, GlobalConfig(Constants.exp, {}, {}, None, None), StageConfig(Constants.stage_id, 'notype', {}, [], [], [], None))

class TestStage:
    def test_abstract(self, filled_corpora):
        with pytest.raises(TypeError):
            Stage(filled_corpora, GlobalConfig(Constants.exp, {}, {}, None, None), StageConfig(Constants.stage_id, 'notype', {}, [], [], [], None))

    def test_init(self, base_stage):
        pass

    def test_get_input_filenames(self, filled_corpora, script_config: StageConfig, filename_gen: FilenameGenerator):
        script_config.type = 'notype'
        stage = InstantiableStage(filled_corpora, GlobalConfig(Constants.exp, {}, {}, None, None), script_config)
        filename_gen.workdir = f'{filename_gen.workdir}/{Constants.corpus_id}'
        input_paths = [
                        filename_gen.get_filename(Constants.first_tag_id, Constants.new_stage_id),
                        filename_gen.get_filename(Constants.second_tag_id, Constants.new_stage_id)
        ]
        output_paths = [
                        filename_gen.get_filename(Constants.first_tag_id, Constants.script_stage_id),
                        filename_gen.get_filename(Constants.second_tag_id, Constants.script_stage_id)
        ]
        assert stage.get_input_filenames(Constants.corpus_id) == input_paths
        assert stage.add_output_filenames(Constants.corpus_id) == output_paths

@pytest.fixture
def script_stage(filled_corpora, global_config, script_config):
    return Script(filled_corpora, global_config, script_config)

def script_command_verifier(command, *args, **kwargs):
    workdir = Constants.workdir
    corpus_id = Constants.corpus_id
    first_tag_id = Constants.first_tag_id
    second_tag_id = Constants.second_tag_id
    new_stage_id = Constants.new_stage_id
    script_stage_id = Constants.script_stage_id
    assert len(command) == 5
    assert command[0] == Constants.script_name
    assert command[1] == f'{workdir}/{corpus_id}/{first_tag_id}/{new_stage_id}'
    assert command[2] == f'{workdir}/{corpus_id}/{second_tag_id}/{new_stage_id}'
    assert command[3] == f'{workdir}/{corpus_id}/{first_tag_id}/{script_stage_id}'
    assert command[4] == f'{workdir}/{corpus_id}/{second_tag_id}/{script_stage_id}'

class SubprocessRunnerMock:
    def __init__(self, verifier) -> None:
        self.verifier = verifier
    def run_and_check_result(self, *args, **kwargs):
        result = None
        self.verifier(*args, **kwargs)
        return result

class TestScript:
    def test_init(self, script_stage):
        assert script_stage.script == Constants.script_name
        
    def test_init_with_scripts_dir(self, filled_corpora, global_config, script_config):
        global_config.scripts = ScriptsConfig('test_dir')
        script_stage = Script(filled_corpora, global_config, script_config)
        assert script_stage.script == f'test_dir/{Constants.script_name}'

    def test_init_fail_when_missing_input_tags(self, filled_corpora, global_config, script_config: StageConfig):
        script_config.input_tags = None
        with pytest.raises(ConfigError):
            Script(filled_corpora, global_config, script_config)

    def test_init_fail_when_missing_output_tags(self, filled_corpora, global_config, script_config: StageConfig):
        script_config.output_tags = None
        with pytest.raises(ConfigError):
            Script(filled_corpora, global_config, script_config)

    def test_run(self, script_stage):
        SubprocessRunnerDependencyInjector.override(SubprocessRunnerMock(script_command_verifier))
        script_stage.run()

corpus = Constants.corpus_id
input_filenames = [f'{Constants.data_input_dir}/{corpus}/{corpus}.en-fr.fr', f'{Constants.data_input_dir}/{corpus}/{corpus}.en-fr.en']
reversed_input_filenames = [f'{Constants.data_input_dir}/{corpus}/{corpus}.fr-en.fr', f'{Constants.data_input_dir}/{corpus}/{corpus}.fr-en.en']

test_input_filename = input_filenames[0]
test_output_filename = f'{Constants.output_dir}/output_file'
class TestSimpleDataTransferUnit:
    def test_transfer(self):
        transfer_unit = SimpleDataTransferUnit()
        transfer_unit.transfer(test_input_filename, test_output_filename)
        with open(test_input_filename, 'r') as origin, open(test_output_filename, 'r') as destination:
            assert origin.read() == destination.read()

default_max_entries = 5
class TestTruncatingDataTransferUnit:
    def test_transfer_long(self):
        max_entries = default_max_entries
        transfer_unit = TruncatingDataTransferUnit(max_entries)
        transfer_unit.transfer(test_input_filename, test_output_filename)
        with open(test_input_filename, 'r') as origin, open(test_output_filename, 'r') as destination:
            origin_lines, destination_lines = origin.readlines(), destination.readlines()
            assert len(destination_lines) == max_entries
            assert destination_lines == origin_lines[:max_entries]

            assert len(destination_lines) <= max_entries
            if len(destination_lines) < max_entries:
                assert len(destination_lines) == len(origin_lines)
                assert destination_lines == origin_lines

    def test_transfer_short(self):
        max_entries = 15
        transfer_unit = TruncatingDataTransferUnit(max_entries)
        transfer_unit.transfer(test_input_filename, test_output_filename)
        with open(test_input_filename, 'r') as origin, open(test_output_filename, 'r') as destination:
            origin_lines, destination_lines = origin.readlines(), destination.readlines()
            input_length = len(origin_lines)
            assert input_length < max_entries # so that the test tests the correct behaviour
            assert len(destination_lines) == len(origin_lines)
            assert destination_lines == origin_lines

class TestMissingDataError:
    def test_init(self):
        message = 'Message'
        corpus = 'Corpus'
        pair = LanguagePair('fr', 'en')
        missing_filename = 'missing_filename.fr-en.fr'
        e = MissingDataError(message, corpus, pair, missing_filename)
        assert e.message == message
        assert e.corpus == corpus
        assert e.pair == pair
        assert e.missing_filename == missing_filename

@pytest.fixture
def modules():
    return {
            'data_query_train': {
                'search_root': Constants.data_input_dir,
                'subpaths': {
                    corpus: f'{corpus}/{corpus}' for corpus in Constants.corpora
                },
                'query': {
                    'src_lgs': ['fr'],
                    'tgt_lgs': ['en'],
                    'max_entries': default_max_entries,
                    'options': ['try_reversed_lg_pairs']
                },
            }
        }

@pytest.fixture
def data_query_config():
    return StageConfig(
        'test_data_query',
        'data_query',
        'train',
        Constants.corpora,
        None,
        [Constants.first_tag_id, Constants.second_tag_id],
        None
    )

def get_mock_path_checker(existing_files):
    def file_exists(path):
        return path in existing_files
    return file_exists

class TestIsValidFile:
    def test_valid(self):
        assert is_valid_file(input_filenames[0])
        
    def test_invalid_path(self):
        assert not is_valid_file('tests/resources/invalid/path/to/file')

    def test_dir(self):
        assert not is_valid_file('tests/resources')

class Observers:
    observers = {}

    @classmethod
    def register(cls, event, f):
        if event not in cls.observers:
            cls.observers[event] = []
        cls.observers[event].append(f)

    @classmethod
    def notify(cls, event, *args, **kwargs):
        if event in cls.observers:
            for observer in cls.observers[event]:
                observer(*args, **kwargs)

class TransferSaves:
    simple_transfers = []
    truncating_transfers = []

    @classmethod
    def erase(cls):
        cls.simple_transfers = []
        cls.truncating_transfers = []

    @classmethod
    def register_simple(cls, origin, destination):
        cls.simple_transfers.append((origin, destination))

    @classmethod
    def register_truncating(cls, default_max_entries, origin, destination):
        cls.truncating_transfers.append((default_max_entries, origin, destination))

Observers.register('simple_transfer', TransferSaves.register_simple)
Observers.register('truncating_transfer', TransferSaves.register_truncating)

class SimpleDataTransferUnitMock(SimpleDataTransferUnit):
    def __init__(self) -> None:
        super().__init__()

    def transfer(self, origin, destination):
        Observers.notify('simple_transfer', origin, destination)

class TruncatingDataTransferUnitMock(TruncatingDataTransferUnit):
    last_call = None

    def __init__(self, default_max_entries) -> None:
        super().__init__(default_max_entries)

    def transfer(self, origin, destination):
        Observers.notify('truncating_transfer', self.max_entries, origin, destination)

class TestDataQuery:
    def test_init_input_tags_refused(self, corpora, global_config):
        with pytest.raises(ConfigError):
            data_query_config = StageConfig(
                'test_data_query',
                'data_query',
                'train',
                None,
                [Constants.first_tag_id],
                [Constants.first_tag_id],
                None
            )
            DataQuery(corpora, global_config, data_query_config)

    def test_fetch(self, corpora, global_config, data_query_config, modules):
        data_query_config.expand(modules)
        data_query_stage = DataQuery(corpora, global_config, data_query_config, get_mock_path_checker(input_filenames))
        file_lists = [data_query_stage.fetch(corpus) for corpus in Constants.corpora]
        assert file_lists == [
            input_filenames
        ]
        # f'{Constants.workdir}/{corpus}/{Constants.first_tag_id}/test_data_query'
        # f'{Constants.workdir}/{corpus}/{Constants.second_tag_id}/test_data_query'
        
    def test_fetch_no_inversion(self, corpora, global_config, data_query_config, modules):
        del modules['data_query_train']['query']['options']
        data_query_config.expand(modules)
        data_query_stage = DataQuery(corpora, global_config, data_query_config, get_mock_path_checker(input_filenames))
        with pytest.raises(MissingDataError):
            try:
                [data_query_stage.fetch(corpus) for corpus in Constants.corpora]
            except MissingDataError as e:
                assert e.missing_filename == reversed_input_filenames[0]
                raise e

        data_query_stage = DataQuery(corpora, global_config, data_query_config, get_mock_path_checker([reversed_input_filenames[0], input_filenames[1]]))
        with pytest.raises(MissingDataError):
            try:
                [data_query_stage.fetch(corpus) for corpus in Constants.corpora]
            except MissingDataError as e:
                assert e.missing_filename == reversed_input_filenames[1]
                raise e

    def test_fetch_missing_source(self, corpora, global_config, data_query_config, modules):
        data_query_config.expand(modules)
        data_query_stage = DataQuery(corpora, global_config, data_query_config, get_mock_path_checker([]))
        with pytest.raises(MissingDataError):
            try:
                [data_query_stage.fetch(corpus) for corpus in Constants.corpora]
            except MissingDataError as e:
                assert e.missing_filename == input_filenames[0]
                raise e

        data_query_stage = DataQuery(corpora, global_config, data_query_config, get_mock_path_checker([reversed_input_filenames[0]]))
        with pytest.raises(MissingDataError):
            try:
                [data_query_stage.fetch(corpus) for corpus in Constants.corpora]
            except MissingDataError as e:
                assert e.missing_filename == input_filenames[1]
                raise e

    def test_run(self, corpora, global_config, data_query_config, modules, filename_gen: FilenameGenerator):
        del modules['data_query_train']['query']['max_entries']
        data_query_config.expand(modules)
        data_query_stage = DataQuery(corpora, global_config, data_query_config)
        DataTransferUnitDependencyInjector.inject(DataTransferMethod.SIMPLE, SimpleDataTransferUnitMock)
        DataTransferUnitDependencyInjector.inject(DataTransferMethod.TRUNCATING, TruncatingDataTransferUnitMock)
        TransferSaves.erase()
        data_query_stage.run()
        filename_gen.workdir = f'{filename_gen.workdir}/{Constants.corpus_id}'
        output_paths = [
                        filename_gen.get_filename(Constants.first_tag_id, 'test_data_query'),
                        filename_gen.get_filename(Constants.second_tag_id, 'test_data_query')
        ]
        print(TransferSaves.simple_transfers)
        print(TransferSaves.truncating_transfers)
        assert TransferSaves.simple_transfers == [(input_filename, output_path) for input_filename, output_path in zip(input_filenames, output_paths)]
        assert TransferSaves.truncating_transfers == []

    def test_run_truncating(self, corpora, global_config, data_query_config, modules, filename_gen: FilenameGenerator):
        data_query_config.expand(modules)
        data_query_stage = DataQuery(corpora, global_config, data_query_config)
        DataTransferUnitDependencyInjector.inject(DataTransferMethod.SIMPLE, SimpleDataTransferUnitMock)
        DataTransferUnitDependencyInjector.inject(DataTransferMethod.TRUNCATING, TruncatingDataTransferUnitMock)
        TransferSaves.erase()
        data_query_stage.run()
        filename_gen.workdir = f'{filename_gen.workdir}/{Constants.corpus_id}'
        output_paths = [
                        filename_gen.get_filename(Constants.first_tag_id, 'test_data_query'),
                        filename_gen.get_filename(Constants.second_tag_id, 'test_data_query')
        ]
        print(TransferSaves.simple_transfers)
        print(TransferSaves.truncating_transfers)
        assert TransferSaves.simple_transfers == []
        assert TransferSaves.truncating_transfers == [(default_max_entries, input_filename, output_path) for input_filename, output_path in zip(input_filenames, output_paths)]

train_data_corpus = 'TrainData'
corpora_names = ['first_corpus', 'second_corpus']
@pytest.fixture
def merge_config():
    return StageConfig(
        'test_merge',
        'merge',
        { 'output_corpus_name': train_data_corpus },
        corpora_names,
        [Constants.first_tag_id, Constants.second_tag_id],
        [Constants.first_tag_id, Constants.second_tag_id],
        None
    )

def prepare_merge_input_files(tags):
    for input_filename, tag in zip(input_filenames, tags):
        os.makedirs(f'tests/outputs/autonmt_test/{train_data_corpus}/{tag}')
        for corpus_name in corpora_names:
            shutil.copyfile(input_filename, f'tests/outputs/autonmt_test/{corpus_name}/{tag}/{Constants.stage_id}')

def verify_merge(tags):
    for tag in tags:
        input_content = ''
        for corpus_name in corpora_names:
            with open(f'tests/outputs/autonmt_test/{corpus_name}/{tag}/{Constants.stage_id}', 'r') as input_file:
                input_content += input_file.read()
        with open(f'tests/outputs/autonmt_test/{train_data_corpus}/{tag}/test_merge') as output_file:
            output_content = output_file.read()
        assert input_content == output_content
        
class TestMerge:
    def test_run(self, corpora: Corpora, global_config, merge_config: StageConfig):
        for corpus_name in corpora_names:
            corpora.new_corpus(corpus_name)
            corpus = corpora.content[corpus_name]
            corpus.add(Constants.first_tag_id, Constants.stage_id)
            corpus.add(Constants.second_tag_id, Constants.stage_id)
        merge_stage = Merge(corpora, global_config, merge_config)
        prepare_merge_input_files(merge_config.input_tags)
        merge_stage.run()
        verify_merge(merge_config.input_tags)


    def test_run_neq_io_number(self, corpora: Corpora, global_config, merge_config: StageConfig):
        for corpus_name in corpora_names:
            corpora.new_corpus(corpus_name)
            corpus = corpora.content[corpus_name]
            corpus.add(Constants.first_tag_id, Constants.stage_id)
            corpus.add(Constants.second_tag_id, Constants.stage_id)
        merge_config.input_tags = [merge_config.input_tags[0]]
        merge_stage = Merge(corpora, global_config, merge_config)
        prepare_merge_input_files(merge_config.input_tags)
        with pytest.raises(ConfigError):
            merge_stage.run()

class BuildVocabCommandVerifier:
    num_calls = 0

    @classmethod
    def init(cls):
        cls.num_calls = 0

    @classmethod
    def verify(cls, command, *args, **kwargs):
        corpus_name = Constants.build_vocab_input_corpora[cls.num_calls]
        assert command[0] == 'onmt-build-vocab'
        assert command[1] == '--save_vocab'
        assert command[2] == f'{Constants.workdir}/{corpus_name}/{Constants.build_vocab_output_tag}/test_build_vocab'
        assert command[3] == '--size'
        assert command[4] == str(Constants.vocab_size)
        assert set(command[5:]) == {
            f'{Constants.workdir}/{corpus_name}/{tag}/{Constants.new_stage_id}'
                for tag in Constants.build_vocab_input_tags
        }
        cls.num_calls += 1

    @classmethod
    def verify_final_state(cls):
        assert cls.num_calls == len(Constants.build_vocab_input_corpora)

class TestBuildVocab:
    def test_init_fail_when_missing_input_tags(self, filled_corpora, global_config, build_vocab_config: StageConfig):
        build_vocab_config.input_tags = None
        with pytest.raises(ConfigError):
            BuildVocab(filled_corpora, global_config, build_vocab_config)

    def test_init_fail_when_missing_output_tags(self, filled_corpora, global_config, build_vocab_config: StageConfig):
        build_vocab_config.output_tags = None
        with pytest.raises(ConfigError):
            BuildVocab(filled_corpora, global_config, build_vocab_config)

    def test_init_fail_when_more_than_one_output_tags(self, filled_corpora, global_config, build_vocab_config: StageConfig):
        build_vocab_config.output_tags = [Constants.first_tag_id, Constants.second_tag_id]
        with pytest.raises(ConfigError):
            BuildVocab(filled_corpora, global_config, build_vocab_config)

    def test_run(self, buildvocab_corpora, global_config, build_vocab_config: StageConfig):
        build_vocab_stage = BuildVocab(buildvocab_corpora, global_config, build_vocab_config)
        BuildVocabCommandVerifier.init()
        SubprocessRunnerDependencyInjector.override(SubprocessRunnerMock(BuildVocabCommandVerifier.verify))
        build_vocab_stage.run()
        BuildVocabCommandVerifier.verify_final_state()
