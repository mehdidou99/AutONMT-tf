import shutil
import os

import pytest
from autonmt.stages.data_query import DataTransferUnitDependencyInjector

from tests.fixtures.corpus import *
from tests.fixtures.configs import *

@pytest.fixture(autouse=True)
def clean_output_dir():
    shutil.rmtree(Constants.output_dir, ignore_errors=True)
    os.makedirs(Constants.output_dir)

@pytest.fixture(autouse=True)
def inject_default_dependencies():
    DataTransferUnitDependencyInjector.back_to_defaults()
