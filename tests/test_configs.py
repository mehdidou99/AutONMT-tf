import pytest

from autonmt.configs import ConfigError, ModelConfig, ScriptsConfig, CacheConfig, GlobalConfig, StageConfig
from tests.constants import Constants
from tests.test_stages import global_config

model_file = 'test_model.py'
model_type = 'TestModelType'
model_config_file = 'test_config.yml'

def check_file_model_config(model_config):
    assert model_config.file == model_file
    assert model_config.config == model_config_file
    assert model_config.type is None

def check_type_model_config(model_config):
    assert model_config.type == model_type
    assert model_config.config == model_config_file
    assert model_config.file is None

class TestModelConfig:
    def test_init_file(self):
        model_config = ModelConfig(model_file, None, model_config_file)
        check_file_model_config(model_config)

    def test_init_type(self):
        model_config = ModelConfig(None, model_type, model_config_file)
        check_type_model_config(model_config)

    def test_init_none(self):
        with pytest.raises(ConfigError):
            ModelConfig(None, None, model_config_file)

    def test_init_both(self):
        with pytest.raises(ConfigError):
            ModelConfig(model_file, model_type, model_config_file)

    def test_from_raw(self):
        model_config = ModelConfig.from_raw({
            'file': model_file,
            'config': model_config_file
        })
        check_file_model_config(model_config)

        model_config = ModelConfig.from_raw({
            'type': model_type,
            'config': model_config_file
        })
        check_type_model_config(model_config)

scripts_dir = 'test_scripts_dir'
class TestScriptsConfig:
    def test_init(self):
        scripts_config = ScriptsConfig(scripts_dir)
        assert scripts_config.dir == scripts_dir

    def test_from_raw(self):
        scripts_config = ScriptsConfig.from_raw({
            'dir': scripts_dir
        })
        assert scripts_config.dir == scripts_dir

    def test_from_raw_none(self):
        assert ScriptsConfig.from_raw(None) is None

cache_dir = 'test_cache_dir'
class TestCacheConfig:
    def test_init(self):
        cache_config = CacheConfig(cache_dir)
        assert cache_config.dir == cache_dir

    def test_from_raw(self):
        cache_config = CacheConfig.from_raw({
            'dir': cache_dir
        })
        assert cache_config.dir == cache_dir

    def test_from_raw_none(self):
        assert CacheConfig.from_raw(None) is None

@pytest.fixture
def simple_model_config():
    return ModelConfig(model_file, None, model_config_file)

@pytest.fixture
def simple_scripts_config():
    return ScriptsConfig(scripts_dir)

@pytest.fixture
def simple_cache_config():
    return ScriptsConfig(cache_dir)

def check_simple_global_config(global_config, simple_model_config, simple_scripts_config, simple_cache_config):
    assert global_config.exp == Constants.exp
    assert global_config.dirs == Constants.dirs
    assert global_config.model.file == simple_model_config.file 
    assert global_config.model.config == simple_model_config.config
    assert global_config.scripts.dir == simple_scripts_config.dir
    assert global_config.cache.dir == simple_cache_config.dir

class TestGlobalConfig:
    def test_init(self, simple_model_config, simple_scripts_config, simple_cache_config):
        global_config = GlobalConfig(Constants.exp, Constants.dirs, simple_model_config, simple_scripts_config, simple_cache_config)
        check_simple_global_config(global_config, simple_model_config, simple_scripts_config, simple_cache_config)

    def test_from_raw(self, simple_model_config, simple_scripts_config, simple_cache_config):
        global_config = GlobalConfig.from_raw({
            'exp': Constants.exp,
            'dirs': Constants.dirs,
            'model': {
                'file': model_file,
                'config': model_config_file
            },
            'scripts': {
                'dir': scripts_dir
            },
            'cache': {
                'dir': cache_dir
            }
        })
        check_simple_global_config(global_config, simple_model_config, simple_scripts_config, simple_cache_config)

def check_simple_stage_config(stage_config: StageConfig):
    assert stage_config.name == Constants.script_stage_id
    assert stage_config.type == 'script'
    assert stage_config.specific['subpath'] == Constants.script_name
    assert stage_config.corpora == [Constants.corpus_id]
    assert stage_config.input_tags == [Constants.first_tag_id, Constants.second_tag_id]
    assert stage_config.output_tags == [Constants.first_tag_id, Constants.second_tag_id]

class TestStageConfig:
    def test_init(self, script_config):
        check_simple_stage_config(script_config)

    def test_from_raw(self, script_config):
        stage_config = StageConfig.from_raw({
            'type': 'script',
            'config': {
                'subpath': Constants.script_name
            },
            'corpora': [Constants.corpus_id],
            'input_tags': [Constants.first_tag_id, Constants.second_tag_id],
            'output_tags': [Constants.first_tag_id, Constants.second_tag_id]
        }, Constants.script_stage_id)
        check_simple_stage_config(stage_config)

    def test_expand(self):
        specific_name = 'specific_script_config'
        stage_config = StageConfig(Constants.script_stage_id,
                        'script',
                        specific_name,
                        [Constants.corpus_id],
                        [Constants.first_tag_id, Constants.second_tag_id],
                        [Constants.first_tag_id, Constants.second_tag_id],
                        None)
        assert stage_config.specific == specific_name
        modules = {
            f'script_{specific_name}': { 'subpath': Constants.script_name }
        }
        stage_config.expand(modules)
        check_simple_stage_config(stage_config)
