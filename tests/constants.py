class Constants:
    data_input_dir = 'tests/resources'
    output_dir = 'tests/outputs'
    workdir = f'{output_dir}/autonmt_test'
    stage_id = 'test_stage'
    first_tag_id = 'test_tag'
    new_stage_id = f'{stage_id}_next'
    second_tag_id = 'test_other_tag'
    corpus_id = 'test_corpus'

    exp = 'test_exp'
    script_stage_id = 'script_stage'
    dirs = ['test_dir', 'other_test_dir']
    script_name = 'test_script.py'

    corpora = [corpus_id]

    build_vocab_input_corpora = ['first_corpus', 'second_corpus']
    build_vocab_input_tags = [first_tag_id, second_tag_id]
    build_vocab_output_tag = 'test_build_vocab_output_tag'
    vocab_size = 65536
