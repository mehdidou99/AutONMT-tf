import subprocess
from sys import stderr

import pytest

from autonmt.util import SubprocessRunner, load_yaml, YamlLoadError, SubprocessError

simple_test = 'tests/resources/simple_test.yml'
invalid_test = 'tests/resources/invalid_test.yml'
not_found_test = 'tests/resources/do_not_exist.yml'

class TestLoadYaml:
    def test_simple_load(self):
        result = load_yaml(simple_test)
        assert result == {
            'foo': [1, 2, 3],
            'bar': {
                'spam': 'hello',
                'eggs': 'world'
            }
        }

    def test_invalid_load(self):
        with pytest.raises(YamlLoadError):
            load_yaml(invalid_test)

    def test_not_found_load(self):
        with pytest.raises(YamlLoadError):
            load_yaml(not_found_test)

class TestSubprocessError:
    def test_init(self):
        message = 'Message'
        result = 'result'
        e = SubprocessError(message, result)
        assert e.message == message
        assert e.result == result

@pytest.fixture
def subprocess_runner():
    return SubprocessRunner()

class TestSubprocessRunner:
    def test_simple_command(self, subprocess_runner: SubprocessRunner):
        command = 'echo'
        arg1 = 'hello'
        arg2 = 'world'
        result = subprocess_runner.run_and_check_result([command, arg1, arg2], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        assert result.stdout.decode('utf-8') == f'{arg1} {arg2}\n'
        assert result.returncode == 0
        assert not result.stderr.decode('utf-8')

    def test_error_command(self, subprocess_runner: SubprocessRunner):
        command = 'cat'
        arg = not_found_test
        with pytest.raises(SubprocessError):
            subprocess_runner.run_and_check_result([command, arg], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        try:
            subprocess_runner.run_and_check_result([command, arg], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        except SubprocessError as e:
            assert e.result.returncode != 0
            assert not e.result.stdout.decode('utf-8')

