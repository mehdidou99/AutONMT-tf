import os

from tests.constants import Constants

workdir = Constants.workdir
tag_id = Constants.first_tag_id
stage_id = Constants.stage_id
new_stage_id = Constants.new_stage_id
second_tag_id = Constants.second_tag_id
corpus_id = Constants.corpus_id

class TestFilenameGenerator:
    def test_init(self, filename_gen):
        assert filename_gen.workdir == workdir

    def test_get_filename(self, filename_gen):
        filename = filename_gen.get_filename(tag_id, stage_id)
        assert filename == f'{workdir}/{tag_id}/{stage_id}'
        assert os.path.exists(f'{workdir}/{tag_id}/')
        assert os.path.isdir(f'{workdir}/{tag_id}/')

class TestTag:    
    def test_add(self, filename_gen, tag):
        filename = tag.add(stage_id)

        assert filename == filename_gen.get_filename(tag_id, stage_id)
        assert os.path.exists(f'{workdir}/{tag_id}/')
        assert os.path.isdir(f'{workdir}/{tag_id}/')

    def test_get(self, filled_tag, filename_gen):
        assert filled_tag.get(stage_id) == filename_gen.get_filename(tag_id, stage_id)
        assert filled_tag.get(new_stage_id) == filename_gen.get_filename(tag_id, new_stage_id)

    def test_get_last(self, filled_tag, filename_gen):
        assert filled_tag.get_last() == filename_gen.get_filename(tag_id, new_stage_id)

class TestCorpus:    
    def test_add(self, filename_gen, corpus):
        assert corpus.add(tag_id, stage_id) == filename_gen.get_filename(tag_id, stage_id)
        assert os.path.exists(f'{workdir}/{tag_id}/')
        assert os.path.isdir(f'{workdir}/{tag_id}/')

        assert corpus.add(tag_id, new_stage_id) == filename_gen.get_filename(tag_id, new_stage_id)
        
        assert corpus.add(second_tag_id, stage_id) == filename_gen.get_filename(second_tag_id, stage_id)
        assert os.path.exists(f'{workdir}/{second_tag_id}/')
        assert os.path.isdir(f'{workdir}/{second_tag_id}/')

    def test_get(self, filled_corpus, filename_gen):
        assert filled_corpus.get(tag_id, stage_id) == filename_gen.get_filename(tag_id, stage_id)
        assert filled_corpus.get(tag_id, new_stage_id) == filename_gen.get_filename(tag_id, new_stage_id)
        assert filled_corpus.get(second_tag_id, stage_id) == filename_gen.get_filename(second_tag_id, stage_id)
        assert filled_corpus.get(second_tag_id, new_stage_id) == filename_gen.get_filename(second_tag_id, new_stage_id)

    def test_get_last(self, filled_corpus, filename_gen):
        assert filled_corpus.get_last(tag_id) == filename_gen.get_filename(tag_id, new_stage_id)
        assert filled_corpus.get_last(second_tag_id) == filename_gen.get_filename(second_tag_id, new_stage_id)

class TestCorpora:
    def test_init(self, corpora):
        assert corpora.workdir == workdir
        assert corpora.content == {}

    def test_new_corpus(self, corpora):
        corpora.new_corpus(corpus_id)
        assert corpus_id in corpora.content.keys()
        assert os.path.exists(f'{workdir}/{corpus_id}/')
        assert os.path.isdir(f'{workdir}/{corpus_id}/')

    def test_creates_files_in_correct_dir(self, filled_corpora):
        assert os.path.exists(f'{workdir}/{corpus_id}/{tag_id}/')
        assert os.path.isdir(f'{workdir}/{corpus_id}/{tag_id}/')
        assert os.path.exists(f'{workdir}/{corpus_id}/{second_tag_id}/')
        assert os.path.isdir(f'{workdir}/{corpus_id}/{second_tag_id}/')
