import pytest

from tests.constants import Constants
from autonmt.configs import StageConfig

@pytest.fixture
def script_config():
    return StageConfig(Constants.script_stage_id,
                        'script',
                        { 'subpath': Constants.script_name },
                        [Constants.corpus_id],
                        [Constants.first_tag_id, Constants.second_tag_id],
                        [Constants.first_tag_id, Constants.second_tag_id],
                        None)

@pytest.fixture
def build_vocab_config():
    return StageConfig('test_build_vocab',
                        'build_vocab',
                        {
                            'size': Constants.vocab_size
                        },
                        Constants.build_vocab_input_corpora,
                        Constants.build_vocab_input_tags,
                        [Constants.build_vocab_output_tag],
                        None)
