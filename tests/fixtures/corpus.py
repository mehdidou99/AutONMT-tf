import shutil

import pytest

from tests.constants import Constants
from autonmt.corpus import FilenameGenerator, Tag, Corpus, Corpora

@pytest.fixture(autouse=True)
def clean_workdir():
    shutil.rmtree(Constants.workdir, ignore_errors=True)

@pytest.fixture
def filename_gen() -> FilenameGenerator:
    return FilenameGenerator(Constants.workdir)

@pytest.fixture
def tag(filename_gen):
    return Tag(Constants.first_tag_id, filename_gen)

@pytest.fixture
def filled_tag(tag):
    tag.add(Constants.stage_id)
    tag.add(Constants.new_stage_id)
    return tag

@pytest.fixture
def corpus(filename_gen):
    return Corpus(filename_gen)

@pytest.fixture
def filled_corpus(corpus):
    corpus.add(Constants.first_tag_id, Constants.stage_id)
    corpus.add(Constants.first_tag_id, Constants.new_stage_id)
    corpus.add(Constants.second_tag_id, Constants.stage_id)
    corpus.add(Constants.second_tag_id, Constants.new_stage_id)
    return corpus

@pytest.fixture
def corpora():
    return Corpora(Constants.workdir)

@pytest.fixture
def filled_corpora(corpora):
    corpora.new_corpus(Constants.corpus_id)
    corpus = corpora.content[Constants.corpus_id]
    corpus.add(Constants.first_tag_id, Constants.stage_id)
    corpus.add(Constants.first_tag_id, Constants.new_stage_id)
    corpus.add(Constants.second_tag_id, Constants.stage_id)
    corpus.add(Constants.second_tag_id, Constants.new_stage_id)
    return corpora

@pytest.fixture
def buildvocab_corpora(corpora):
    for corpus_name in Constants.build_vocab_input_corpora:
        corpora.new_corpus(corpus_name)
        corpus = corpora.content[corpus_name]
        corpus.add(Constants.first_tag_id, Constants.stage_id)
        corpus.add(Constants.first_tag_id, Constants.new_stage_id)
        corpus.add(Constants.second_tag_id, Constants.stage_id)
        corpus.add(Constants.second_tag_id, Constants.new_stage_id)
    return corpora
