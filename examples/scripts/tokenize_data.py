#!/usr/bin/env python3

import subprocess
import sys

def stderr_print(*args, **kwargs):
    print(*args, **kwargs, file=sys.stderr)

args = sys.argv[1:]
num_args = len(args)
assert num_args % 2 == 1
half_index = (num_args-1) // 2
input_filenames = args[:half_index]
output_filenames = args[half_index:-1]
bpe_output_filename = args[-1]

stderr_print('Tokenizing generated data')
stderr_print('Training tokenizer')
learn_command = '/nfs/SSALING-DATA/michon/Tokenizer/build/cli/subword_learn --mode conservative'.split(' ')
inputs = [['-i', input_filename] for input_filename in input_filenames]
inputs = [arg for pair in inputs for arg in pair]
output = ['-o', bpe_output_filename]
trailing_options = '-- bpe --symbols 32000'.split(' ')
result = subprocess.run(learn_command + inputs + output + trailing_options)
if result.returncode != 0:
    stderr_print('Error during tokenizer training')
    sys.exit(1)

stderr_print('Tokenizing')
tokenize_command = '/nfs/SSALING-DATA/michon/Tokenizer/build/cli/tokenize --mode conservative --joiner_annotate'.split(' ')
tokenize_command += ['--bpe_model', bpe_output_filename]

for input_filename, output_filename in zip(input_filenames, output_filenames):
    with open(input_filename, 'rb') as tokenizer_input, \
            open(output_filename, 'wb') as tokenizer_output:
        result = subprocess.run(tokenize_command, stdin=tokenizer_input, stdout=tokenizer_output)
        if result.returncode != 0:
            stderr_print(f'Error during tokenization of file {input_filename}')
            sys.exit(1)
