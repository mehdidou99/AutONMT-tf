#!/usr/bin/env python3

import sys

src_filename, tgt_filename, src_out_filename, tgt_out_filename = sys.argv[1:]

with open(src_filename, 'r') as f_src, open(tgt_filename, 'r') as f_tgt, \
        open(src_out_filename, 'w') as f_src_out, open(tgt_out_filename, 'w') as f_tgt_out:
    for line_src, line_tgt in zip(f_src, f_tgt):
        line_src = f'｟subtitle｠ {line_src}'.replace(' <eol>', '').replace(' <eob>', '')
        line_tgt = line_tgt.replace('<eol>', '｟eol｠').replace('<eob>', '｟eob｠')

        f_src_out.write(line_src)
        f_tgt_out.write(line_tgt)
